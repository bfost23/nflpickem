"use strict";
Router.route('/', {
    template: "home",
    name: "home"
    });
Router.route('/week1', {
    template: "week1",
    name: "week1"
    });
Router.route('/week1results', {
    template: "week1results",
    name: "week1results"
    });
Router.route('/week2', {
    template: "week2",
    name: "week2"
    });
Router.route('/week3', {
    template: "week3",
    name: "week3"
    });
Router.route('/week4', {
    template: "week4",
    name: "week4"
    });
Router.route('/week5', {
    template: "week5",
    name: "week5"
    });
Router.route('/week6', {
    template: "week6",
    name: "week6"
    });
Router.route('/profile', {
    template: "profile",
    name: "profile"
    });
Router.route('/leaderboard', {
    template: "leaderboard",
    name: "leaderboard"
    });